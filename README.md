# Pantheon For Slackware 

This project is based on 

[Pantheon DE](https://github.com/elementary)<br>
_designed and developed for elementary OS_<br><br>
[GFS Project](https://github.com/slackport/gfs)<br>
_Gnome 40 for Slackware_<br><br>
[SlackBuilds Team](https://slackbuilds.org/)<br>
_A community-driven repository for Slackware users_<br><br>
[Slackware 15](https://docs.slackware.com/slackware:current)<br>
_The Slackware Linux Project_<br><br><br>

<h2>Download and Install Compiled X86_64 Packages</h2>

**1. Download**
```
lftp -c mirror https://slackernet.ddns.net/slackware/slackware64-current/slackware64/gnome/ -c gnome-x86_64
lftp -c mirror https://slackernet.ddns.net/slackware/slackware64-current/slackware64/pantheon/ -c pantheon-x86_64
```

**2. Install as root**
```
upgradepkg --install-new --reinstall gnome-x86_64/*.t?z
upgradepkg --install-new --reinstall pantheon-x86_64/*.t?z
```

<h2>Configuration</h2>

1. We need to add some groups and users for our Pantheon DE:

```
groupadd -g 214 avahi
useradd -u 214 -g 214 -c "Avahi User" -d /dev/null -s /bin/false avahi
groupadd -g 303 colord
useradd -d /var/lib/colord -u 303 -g colord -s /bin/false colord
groupadd -g 363 sanlock
useradd -u 363 -d /var/run/sanlock -s /bin/false -g sanlock sanlock
usermod -a -G disk sanlock
groupadd -g 319 rabbitmq
useradd -u 319 -g 319 -c "Rabbit MQ" -d /var/lib/rabbitmq -s /bin/sh rabbitmq
groupadd -g 365 lightdm
useradd  -c "Lightdm Daemon" -d /var/lib/lightdm -u 365 -g lightdm -s /bin/false lightdm
```

**2. Avahi needs to be run at boot, so edit your /etc/rc.d/rc.local adding these lines:**

```
# Start avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
 /etc/rc.d/rc.avahidaemon start
fi
# Start avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd start
fi
```

**4. Also stop Avahi at shutdown, so edit your /etc/rc.d/rc.local_shutdown adding these lines:**

```
# Stop avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd stop
fi
# Stop avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
  /etc/rc.d/rc.avahidaemon stop
fi
```

**5. We also need to mark our new created /etc/rc.d/rc.local_shutdown file as executable**

`chmod +x /etc/rc.d/rc.local_shutdown`

**6. Edit your /etc/inittab to go 4 runlevel**

from:
`id:3:initdefault:`
to
`id:4:initdefault:`

**7. Add lightdm and make sure lightdm is the first one to run in the /etc/rc.d/rc.4**

```
# to use lightdm  by default:
if [ -x /usr/bin/lightdm ]; then
  exec /usr/bin/lightdm
fi
```
**8. Edit your /etc/lightdm/lightdm.conf file to use Pantheon greeter by default.**

In the `[Seat:*]` section uncomment and change<br>
<br>`#greeter-session=example-gtk-gnome`<br>to<br>`greeter-session=io.elementary.greeter`<br><br>

<h2>If you would like to compile Pantheon as a desktop environment</h2> 

Add required users and groups, specified in the step 1 configuration section.

```
git clone https://gitlab.com/slackernetuk/gfs.git -b 15-20211202
cd gfs
sh gfs
```
```
git clone https://gitlab.com/slackernetuk/pantheon-for-slackware.git -b stable
cd pantheon-for-slackware
sh install.sh
```
....then continue with step 2 of configuration section.

<h2>Screenshot</h2>
![Screenshot_from_2021-12-20_06-08-57](/uploads/99eb60c4f2595ddcac9d69bc3ca407b1/Screenshot_from_2021-12-20_06-08-57.png)

<h2>Thanks</h2>
[ElementaryOS Team](https://github.com/elementary)<br>
[GFS Project](https://github.com/slackport/gfs)<br>
[SlackBuilds Team](https://slackbuilds.org/)

<h2>Contact</h2>
slackernetuk@gmail.com










