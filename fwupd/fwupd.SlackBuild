#!/bin/sh

# Slackware build script for fwupd

# Copyright 2021 Frank Honolka <slackernetuk@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=fwupd
VERSION=${VERSION:-1.7.3}
BUILD=${BUILD:-1}
TAG=${TAG:-_snuk}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/snuk}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/var/cache/snuk}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.xz
cd $PRGNAM-$VERSION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

  CFLAGS="$SLKCFLAGS" \
  CXXFLAGS="$SLKCFLAGS" \
  meson build \
    --buildtype=release \
    --libdir=/usr/lib${LIBDIRSUFFIX} \
    --libexecdir=/usr/libexec \
    --localstatedir=/var \
    --mandir=/usr/man \
    --prefix=/usr \
    --sysconfdir=/etc \
    -Delogind=true \
    -Db_lto=false \
    -Dplugin_modem_manager=true \
    -Dplugin_intel_spi=true \
    -Dlzma=true \
    -Dplugin_flashrom=true \
    -Dsupported_build=true \
    -Defi_binary=false \
    -Dplugin_upower=true \
    -Ddocs=none \
    -Dgusb:docs=false \
    -Dgcab:docs=false \
    -Dsystemd=false
  meson compile -C build  
  DESTDIR=$PKG meson install -C build 

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

mv $PKG/etc/pki/fwupd/GPG-KEY-Linux-Foundation-Firmware $PKG/etc/pki/fwupd/GPG-KEY-Linux-Foundation-Firmware.new || exit 1
mv $PKG/etc/pki/fwupd/GPG-KEY-Linux-Vendor-Firmware-Service $PKG/etc/pki/fwupd/GPG-KEY-Linux-Vendor-Firmware-Service.new || exit 1
mv $PKG/etc/pki/fwupd/LVFS-CA.pem $PKG/etc/pki/fwupd/LVFS-CA.pem.new || exit 1
mv $PKG/etc/pki/fwupd-metadata/GPG-KEY-Linux-Foundation-Metadata $PKG/etc/pki/fwupd-metadata/GPG-KEY-Linux-Foundation-Metadata.new || exit 1
mv $PKG/etc/pki/fwupd-metadata/GPG-KEY-Linux-Vendor-Firmware-Service $PKG/etc/pki/fwupd-metadata/GPG-KEY-Linux-Vendor-Firmware-Service.new || exit 1
mv $PKG/etc/pki/fwupd-metadata/LVFS-CA.pem $PKG/etc/pki/fwupd-metadata/LVFS-CA.pem.new || exit 1
mv $PKG/etc/fwupd/remotes.d/lvfs.conf $PKG/etc/fwupd/remotes.d/lvfs.conf.new || exit 1
mv $PKG/etc/fwupd/remotes.d/lvfs-testing.conf $PKG/etc/fwupd/remotes.d/lvfs-testing.conf.new || exit 1
mv $PKG/etc/fwupd/remotes.d/vendor.conf $PKG/etc/fwupd/remotes.d/vendor.conf.new || exit 1
mv $PKG/etc/fwupd/remotes.d/vendor-directory.conf $PKG/etc/fwupd/remotes.d/vendor-directory.conf.new || exit 1
mv $PKG/etc/fwupd/remotes.d/fwupd-tests.conf $PKG/etc/fwupd/remotes.d/fwupd-tests.conf.new || exit 1
mv $PKG/etc/fwupd/daemon.conf $PKG/etc/fwupd/daemon.conf.new || exit 1
mv $PKG/etc/fwupd/redfish.conf $PKG/etc/fwupd/redfish.conf.new || exit 1
mv $PKG/etc/fwupd/thunderbolt.conf $PKG/etc/fwupd/thunderbolt.conf.new || exit 1
mv $PKG/etc/fwupd/uefi_capsule.conf $PKG/etc/fwupd/uefi_capsule.conf.new || exit 1

rm -rf $PKG/usr/share/installed-tests || exit 1

mkdir -p $PKG/usr/share/dbus-1-system-services || exit 1

mkdir -p $PKG/usr/share/dbus-1/system-services
sed 's|@libexecdir@|/usr/libexec|' data/org.freedesktop.fwupd.service.in > $PKG/usr/share/dbus-1/system-services/org.freedesktop.fwupd.service || exit 1

find $PKG -name perllocal.pod \
  -o -name ".packlist" \
  -o -name "*.bs" \
  | xargs rm -f

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
DOCS="AUTHORS CODE_OF_CONDUCT.md COMMITMENT CONTRIBUTING.md COPYING MAINTAINERS RELEASE README.md SECURITY.md"
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
/sbin/upgradepkg --install-new --reinstall $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
